class Integer
   
    def to_ba(size=8)
      a=[]
      (size-1).downto(0) do |i|
        a<<self[i]
      end
      a
    end

end

class Board
    
    def initialize(matrix, horizontal, vertical)
        @matrix = matrix
        @size = matrix.size
        @horizontal = horizontal
        @vertical = vertical
        @solutions = { horizontal: Array.new(@size) {Array.new}, vertical: Array.new(@size) {Array.new} }
    end   

    def solve
        find_possible_solutions
        result = solution(@solutions[:horizontal], 0)
        return "No solutions found" if !result
        result = result.map(&:first).transpose
        s = ""
        @matrix.each_with_index do |matrix_row, row|
            matrix_row.each_with_index do |number, column|
                s += (number*result[row][column]).to_s
            end
            s += "\n"
        end
        s
    end    

    private
    
    def solution(columns, row)
        return columns if (row == @size)
        @solutions[:vertical][row].each do |variant|
            variant_columns = (0...@size).map do |column|
                columns[column].select do |h| 
                    h[row] == variant[column]
                end
            end
            next if no_possible_solutions?(variant_columns) #no possible columns with this row variant
            @result = solution(variant_columns, row+1)
        end
        return @result
    end

    def no_possible_solutions?(columns)
        columns.reduce(false) { |t, h| t || h.empty?  }
    end

    def row_sum(row, variant = [1]*@size, matrix = @matrix)
        matrix[row].each_with_index.inject(0) do |sum, (number, column)| 
            sum + number*variant[column]
        end
    end

    def column_sum(column, variant = [1]*@size, matrix = @matrix)
        matrix.each_with_index.inject(0) do |sum, (numbers, row)| 
            sum + numbers[column]*variant[row]
        end
    end

    def find_possible_solutions
        (1...2**@size).map { |n| n.to_ba(@size) }.each do |variant|
            (0...@size).each do |i|
                if(row_sum(i, variant) == @vertical[i])
                    @solutions[:vertical][i] << variant
                end

                if(column_sum(i, variant) == @horizontal[i])
                    @solutions[:horizontal][i] << variant
                end
            end
        end
    end

end

if !(input_file = ARGV[0])
    puts "No input file" 
    return
end

f = File.open(input_file, "r")
matrix = []
f.each_line do |line|
    matrix << line.split.map(&:to_i)
end
matrix.reject!(&:empty?)
vertical, horizontal = matrix.pop, matrix.pop
input = Board.new(matrix, horizontal, vertical)

puts input.solve