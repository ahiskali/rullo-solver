# Rullo solver

Solver for [this game](https://play.google.com/store/apps/details?id=air.com.akkad.rullo&hl=en).

## Usage
Create a file with inside matrix and then add horizontal and vertical values at the bottom.

```
4 4 2 4 4 2 4 2
3 3 3 2 4 3 2 2
2 4 2 2 2 3 4 3
4 4 4 4 4 4 3 4
3 2 4 4 4 2 4 3
3 4 2 4 3 3 2 2
2 2 4 3 2 4 3 3
3 4 2 3 4 3 4 2
14 24 17 22 19 21 19 9
22 13 16 27 19 16 17 15
```

Run `ruby rullo.rb input.txt`